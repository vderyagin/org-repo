;;; org-repo.el --- Code for managing a set of org files -*- lexical-binding: t -*-

;; Copyright (C) 2016-2022 Victor Deryagin

;; Author: Victor Deryagin <vderyagin@gmail.com>
;; Maintainer: Victor Deryagin <vderyagin@gmail.com>
;; Created: 25 Jul 2016
;; Version: 0.5.1

;; Package-Requires: ()

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;;; Code:

(require 'seq)
(require 'subr-x)

(defgroup org-repo nil
  "Code for managing a set of org files"
  :group 'org)

(defcustom org-repo-url nil
  "URL for git repository containing org files."
  :group 'org-repo
  :type '(string :tag "Git repository URI"))

(defcustom org-repo-sync-new-files nil
  "Whether to auto-commit & sync previously uncommited files"
  :group 'org-repo
  :type 'boolean)

(defcustom org-repo-dir (expand-file-name "~/org/")
  "Path to directory for storing org files locally"
  :group 'org-repo
  :type 'directory)

(defcustom org-repo-before-clone-hook nil
  "Hook that gets run before attempt to clone the org repo.

Can be used to, e.g., mount remote filesystem where org repo
directory is stored."
  :group 'org-repo
  :type 'hook)

(defcustom org-repo-after-sync-hook nil
  "Hook that gets run after repo sync."
  :group 'org-repo
  :type 'hook)

;;;###autoload
(defun org-repo-init ()
  (interactive)
  (org-repo-clone-if-missing)
  (let ((repo-init-file (expand-file-name ".org-repo-init" org-repo-dir)))
    (load repo-init-file 'noerror)))

(defun org-repo-configure-git-options (dir)
  (unless (file-directory-p dir)
    (user-error "'%s' is not a valid directory" (abbreviate-file-name dir)))
  (unless (org-repo-git-repo-p dir)
    (user-error "'%s' is not a valid git repository" (abbreviate-file-name dir)))
  (let ((default-directory (file-name-as-directory dir)))
    (when org-repo-sync-new-files
      (call-process "git" nil nil nil
                    "config" "--bool" "branch.master.syncNewFiles" "true"))
    (call-process "git" nil nil nil
                  "config" "--bool" "branch.master.sync" "true")))

(defun org-repo-git-repo-p (dir)
  (and
   (file-directory-p dir)
   (let ((default-directory (file-name-as-directory dir)))
     (thread-last "git rev-parse --is-inside-work-tree"
       shell-command-to-string
       string-trim
       (equal "true")))))

;;;###autoload
(defun org-repo-clone-if-missing ()
  (run-hooks 'org-repo-before-clone-hook)
  (unless (org-repo-git-repo-p org-repo-dir)
    (unless (y-or-n-p "No org files. Get them? ")
      (error "Aborted"))
    (org-repo-clone-repo org-repo-dir org-repo-url)))

(defun org-repo-clone-repo (dir uri)
  (unless (org-repo-git-repo-p dir)
    (org-repo-git-clone dir uri)
    (org-repo-configure-git-options dir)
    (org-repo-git-gc dir)))

(defun org-repo-git-clone (dir uri)
  (let ((progress (make-progress-reporter
                   (format "Cloning a repository '%s' to '%s': " uri (abbreviate-file-name dir))))
        (process (start-process "org-repo-git-clone" nil
                                "git" "clone" uri dir)))

    (while (process-live-p process)
      (sleep-for 0.2)
      (progress-reporter-update progress))

    (progress-reporter-done progress)

    (unless (zerop (process-exit-status process))
      (error "Failed to clone a repository '%s' to '%s'" uri (abbreviate-file-name dir)))))

(defun org-repo-git-gc (dir)
  (let ((default-directory (file-name-as-directory dir)))
    (let ((progress (make-progress-reporter "GC-ing a newly cloned repository: "))
          (process (start-process "git-gc-org" nil
                                  "git" "gc" "--prune=now" "--aggressive")))
      (while (process-live-p process)
        (sleep-for 0.2)
        (progress-reporter-update progress))
      (progress-reporter-done progress))))

;;;###autoload
(defun org-repo-remove ()
  (interactive)
  (unless (file-directory-p org-repo-dir)
    (user-error "No org directory"))
  (unless (org-repo-git-repo-p org-repo-dir)
    (user-error "'%s' is not a valid git repository"
                (abbreviate-file-name org-repo-dir)))
  (unless (y-or-n-p "Remove org files? (will be synchronized before removal) ")
    (error "Aborted"))
  (org-repo-kill-visiting-buffers)
  (compile (string-join (list (org-repo-sync-command)
                              (org-repo-remove-files-in-dir-command org-repo-dir))
                        " && ")))

(defun org-repo-kill-visiting-buffers ()
  (save-some-buffers)
  (thread-last (buffer-list)
    (seq-map #'buffer-file-name)
    (seq-filter (lambda (file)
                  (and file
                       (string-prefix-p (expand-file-name org-repo-dir) file))))
    (seq-each #'kill-buffer)))

;;;###autoload
(defun org-repo-sync ()
  (interactive)
  (unless (executable-find "git-sync")
    (error "You must have git-sync (https://github.com/simonthum/git-sync) installed to perform this action"))
  (save-some-buffers)
  (unless (org-repo-git-repo-p org-repo-dir)
    (org-repo-clone-repo org-repo-dir org-repo-url))
  (org-repo-configure-git-options org-repo-dir)
  (let* ((default-directory (expand-file-name org-repo-dir))
         (buf-name "*org-repo-sync*")
         (process (prog1 (start-process "org-repo-sync" buf-name  "git" "sync")
                    (pop-to-buffer buf-name)))
         (sentinel (lambda (_p event)
                     (when (string-prefix-p "finished" event)
                       (run-hooks 'org-repo-after-sync-hook)))))
    (set-process-sentinel process sentinel)))

(defun org-repo-sync-command ()
  (thread-last org-repo-dir
    expand-file-name
    shell-quote-argument
    (format "cd %s && git sync")))

(defun org-repo-remove-files-in-dir-command (dir)
  (thread-last dir
    expand-file-name
    shell-quote-argument
    (format "rm --force --recursive --verbose %s/{*,.*}")))

;;;###autoload
(defun org-repo-search ()
  (interactive)
  (org-repo-clone-if-missing)
  (let ((re (read-from-minibuffer "Search for: "))
        (default-directory (file-name-as-directory org-repo-dir)))
    (grep-compute-defaults)
    (rgrep re "*.org" default-directory)))

(provide 'org-repo)

;;; org-repo.el ends here
