# org-repo #

Some code for managing a set of org files.

## Dependencies ##

* [git][1]
* [org-sync][2]

[1]: https://git-scm.com "Git"
[2]: https://github.com/simonthum/git-sync "Safe and simple one-script git synchronization"

## Usage ##
